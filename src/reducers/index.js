import { combineReducers } from "redux";
import createReducer from "../create/reducers/createReducer";
import homeReducer from "../home/reducers/homeReducer";
import detailsReducer from "../details/reducers/detailsReducer";
import marksReducer from "./marksReducer";

const reducers = combineReducers({
  createReducer,
  homeReducer,
  detailsReducer,
  marksReducer
});
export default reducers;
