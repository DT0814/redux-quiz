import { fetchDataByDelete } from "../../utils/FetchData";

export const changeCurrentId = (currentId) => (dispatch) => {
  dispatch({
    type: 'CHANGE_CURRENT_ID',
    currentId: currentId
  });
};

export const deleteMarkDown = (id) => (dispatch) => {
  fetchDataByDelete(`http://localhost:8080/api/posts/${id}`)
    .then(result => {
      console.log(result)
      dispatch({
        type: 'DELETE_MARKDOWN_BY_ID',
        result: result
      });
  });
};
