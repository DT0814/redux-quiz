import React, { Fragment } from 'react'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { deleteMarkDown } from "../actions/detailsActions";

export class RightDiv extends React.Component {
  handlerBackHome() {
    window.location.href = '/';
  }

  deleteMarkDown(event) {
    this.props.deleteMarkDown(event.target.id);
    window.location.href = '/';
  }

  render() {
    const currentId = this.props.detailsReducer.currentId;
    const marks = this.props.marksReducer.markDowns;
    return (
      <Fragment>
        {
          marks.map((value) => {
            if (Number.parseInt(value.id) === Number.parseInt(currentId)) {
              return (
                <div className='right-div'>
                  <h1>{value.title}</h1>
                  <hr/>
                  <p>{value.description}</p>
                  <button onClick={this.deleteMarkDown.bind(this)} id={value.id} className='delete-button'>删除</button>
                  <button onClick={this.handlerBackHome.bind(this)} className='cancel-but'>取消</button>
                </div>
              )
            }
          })
        }
      </Fragment>
    )
  }
}

const mapStateToProps = state => ({
  detailsReducer: state.detailsReducer,
  marksReducer: state.marksReducer
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    deleteMarkDown
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(RightDiv);
