import React from 'react'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { changeCurrentId } from "../actions/detailsActions";

export class LeftMenu extends React.Component {
  handlerChangeCurrentId(event) {
    this.props.changeCurrentId(event.target.id);

  }

  render() {
    const currentId = this.props.detailsReducer.currentId;
    const marks = this.props.marksReducer.markDowns;
    console.log(currentId);
    console.log(marks)
    return (
      <div className='left-menu'>
        <ul>
          {
            marks.map((value, index) => {
              let res;
              Number.parseInt(value.id) === Number.parseInt(currentId)
                ? res = <li id={value.id} onClick={this.handlerChangeCurrentId.bind(this)}
                            className={'selected'}>{value.title}</li>
                : res = <li id={value.id} onClick={this.handlerChangeCurrentId.bind(this)}>{value.title}</li>;
              return (
                res
              )
            })
          }
        </ul>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  detailsReducer: state.detailsReducer,
  marksReducer: state.marksReducer
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    changeCurrentId
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(LeftMenu);
