import React from 'react'
import './details.less'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LeftMenu from "../componets/LeftMenu";
import { changeCurrentId } from "../actions/detailsActions";
import  RightDiv  from "../componets/RightDiv";

export class Details extends React.Component {

  componentDidMount() {

  }

  constructor(props) {
    super(props);
    this.props.changeCurrentId(this.props.match.params.id);
  }

  render() {
    return (
      <div className='details-body'>
        <LeftMenu/>
        <RightDiv/>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  detailsReducer: state.detailsReducer,
  marksReducer: state.marksReducer
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    changeCurrentId
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Details);

