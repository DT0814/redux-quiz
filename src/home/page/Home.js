import React from 'react'
import './home.less'
import { bindActionCreators } from "redux";
import { getAll } from "../actions/homeActions";
import { connect } from "react-redux";
import Show from "../componets/Show";
import { Link } from "react-router-dom";

export class Home extends React.Component {

  componentDidMount() {
    this.props.getAll();
  }

  render() {

    return (
      <div className={'home-body'}>
        <Show marks={this.props.homeReducer.markDowns}/>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  homeReducer: state.homeReducer
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    getAll
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);

