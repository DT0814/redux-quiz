import { fetchDataByGet } from "../../utils/FetchData";

export const getAll = () => (dispatch) => {
  return fetchDataByGet('http://localhost:8080/api/posts')
    .then(result => {
      console.log(result);
      dispatch({
        type: 'GET_ALL_MARKDOWN',
        markDowns: result
      });
    })
};
