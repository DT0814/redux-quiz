import { fetchDataByPost } from "../../utils/FetchData";

export const commitMarkDown = (state) => (dispatch) => {
  const body = {
    title: state.title,
    description: state.description
  };
  fetchDataByPost('http://localhost:8080/api/posts', body).then(result => {
    dispatch({
      type: 'COMMIT_MARKDOWN',
      commitResult: result
    });
  }).catch(error => {
    console.log('失败', error)
  });
};

export const changeTitle = (title) => (dispatch) => {
  dispatch({
    type: 'CHANGE_TITLE',
    title: title
  });
};

export const changeDescription = (description) => (dispatch) => {
  dispatch({
    type: 'CHANGE_DESCRIPTION',
    description: description
  });
};

export const changeCommitResult = (result) => (dispatch) => {
  dispatch({
    type: 'CHANGE_COMMIT_RESULT',
    commitResult: result
  });
};
