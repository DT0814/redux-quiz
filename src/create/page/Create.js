import React from 'react'
import './create.less'
import Button from "../componets/Button";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { changeCommitResult, changeDescription, changeTitle, commitMarkDown } from "../actions/createActions";

export class Create extends React.Component {
  handlerChangeTitle(event) {
    this.props.changeTitle(event.target.value)
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.createReducer.commitResult) {
      this.props.changeCommitResult(false);
      window.location.href = '/';
    }
  }

  handlerChangeDescription(event) {
    this.props.changeDescription(event.target.value)
  }


  render() {
    return (
      <div className='create-body'>
        <h2>创建笔记</h2>
        <hr/>
        <label>标题</label>
        <br/>
        <input data-testId={'title-input'} onChange={this.handlerChangeTitle.bind(this)} className='create-title' type="text"/>
        <label>正文</label>
        <textarea data-testId={'description-input'}  cols="105 " rows="10" onChange={this.handlerChangeDescription.bind(this)}/>
        <Button data-testId={'commit-but'}  createReducer={this.props.createReducer} isDisable={this.props.createReducer.commitDisable}
                commitMarkDown={this.props.commitMarkDown} butClass={'commit-but'} name={'提交'}/>
        <Button isDsiable={false} butClass={'cancel-but'} name={'取消'}/>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  createReducer: state.createReducer
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    commitMarkDown,
    changeTitle,
    changeDescription,
    changeCommitResult
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Create);

