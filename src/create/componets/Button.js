import React from 'react'

export default class Button extends React.Component {
  handlerClick() {
    if (this.props.name === '提交') {
      this.props.commitMarkDown(this.props.createReducer);
    }
    if (this.props.name === '取消') {
      window.location.href = '/';
    }
  }

  render() {
    return (
      <button onClick={this.handlerClick.bind(this)} disabled={this.props.isDisable}
              className={this.props.butClass}>{this.props.name}</button>
    )
  }
}
