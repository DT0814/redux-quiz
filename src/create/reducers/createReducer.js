const initState = {
  commitResult: false,
  title: '',
  description: '',
  commitDisable: true
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'COMMIT_MARKDOWN':
      return {
        ...state,
        commitResult: action.commitResult
      };
    case 'CHANGE_TITLE':
      return {
        ...state,
        title: action.title,
        commitDisable: checkDisable(state.title, state.description)
      };
    case 'CHANGE_DESCRIPTION':
      return {
        ...state,
        description: action.description,
        commitDisable: checkDisable(state.title, state.description)
      };
    case 'CHANGE_COMMIT_RESULT': {
      return {
        ...state,
        commitResult: action.commitResult,
      };
    }
    default:
      return state
  }
};
const checkDisable = (title, description) => {
  return title === '' || description === '' || checkLength(title, description);
};

const checkLength = (title, description) => {
  return title.length > 128 || description.length > 65536;
};
