import React, { Component } from 'react';
import './App.less';
import { BrowserRouter as Router } from "react-router-dom";
import { Route, Switch } from "react-router";
import Create from "./create/page/Create";
import Home from "./home/page/Home";
import Details from "./details/page/Details";
import homeImg from './resource/home.png'
class App extends Component {

  render() {
    return (
      <Router>
        <div className='header-div' >
          <img src={homeImg} alt="home"/>
              <span>NOTES</span>
        </div>
        <Switch>
          <Route path={'/notes/create'} component={Create}/>
          <Route path={'/notes/:id'} component={Details}/>
          <Route path={'/'} component={Home}/>
        </Switch>
      </Router>
    );
  }
}

export default App;
